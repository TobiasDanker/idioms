<?php 
//include('../../../config/glancrConfig.php');

$idiomInterval = getConfigValue('idiom_interval');
// wenn der parameter noch nicht gesetzt wurde
if(empty($idiomInterval)) {
	$idiomInterval = 1;
}
?>


var dummy_parameter;

$(document).ready(function () {
	getIdiom();
	console.log('<?php echo $idiomInterval;?>');
	setInterval(getIdiom, <?php echo $idiomInterval;?> * 60000);
});

function getIdiom() {
	$.ajax({
		dataType: 'json',
		url: '../modules/idioms/assets/getIdiom.php',
		success: function(data) {
			console.log('da: ' + data);
			$("#idiom_container").empty();
			$("#idiom_container").append("<p>" + data.replace('|', '</p><span>') + "</span>");
	   }
	});	
}



