<?php 
$idiomInterval = getConfigValue('idioms_interval');
// wenn der parameter noch nicht gesetzt wurde
if(empty($idiomInterval)) {
	$idiomInterval = 1;
}
?>
<div class="idiom-interval__minutes">
	<?php echo _('interval') . ' ( ' . _('minutes');?>):
	<input id="idiom-interval__minutes--set" type="number" value="<?php echo $idiomInterval;?>"/>
</div>
<div style="height:0;">
	<div class="validate" id="idiom-invalid_interval"><?php echo _('invalid interval');?></div>
</div>
<div class="block__add" id="idiom-interval__edit">
	<button class="idiom_interval__edit--button" href="#">
		<span><?php echo _('save'); ?></span>
	</button>
</div>
