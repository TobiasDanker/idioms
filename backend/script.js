$('#idiom-interval__edit').click(function() {
	var newValue = $( "#idiom-interval__minutes--set").val();
	if(newValue < 1 || newValue > 120) {
		$("#idiom-invalid_interval").show();
	} else {
		$.post('setConfigValueAjax.php', {'key' : 'idioms_interval', 'value' : newValue})
		.done(function() { 
			$('#ok').show(30, function() {
				$(this).hide('slow');
			})
		});
	}
});
var oldValue = ""; 
$( "#idiom-interval__minutes--set").change(function() {
	$("#idiom-invalid_interval").hide();
});
$( "#idiom-interval__minutes--set").keydown(function() {
	$("#idiom-invalid_interval").hide();
	oldValue= $(this).val();
});
$( "#idiom-interval__minutes--set").keyup(function() {
	if(!isNaturalNumber ($(this).val() )){ //Only numbers !
		$(this).val(oldValue) ; 
	} 
});
function isNaturalNumber(n) {
    n = n.toString(); // force the value incase it is not
    var n1 = Math.abs(n),
        n2 = parseInt(n, 10);
    return !isNaN(n1) && n2 === n1 && n1.toString() === n;
}
